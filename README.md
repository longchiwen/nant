Npm package for Nant
=========
Package contains Nant binaries and node module wrapper. [Check out Nant on sourceforge.](http://nant.sourceforge.net/)

Nant version: 0.9.2


## Install

```shell
npm install @longchiwen/nant --save
```

## Use from node

```shell
var nant = require("@longchiwen/nant");
nant.run(["-help"]);
```
Function returns Promise.

## Use from cmd

```shell
lcw-nant -help
```

## Test

Checkout from repo first.

```shell
npm test
```

## Release History

* 0.9.4 Added node module wrapper
* 0.9.3 Config changes
* 0.9.2 Initial release