var Build = require("./index.js"),
	process = require("process"),
	assert = require("assert"),
	colors = require("colors"),
	fs = require('fs');

function fileExists(filePath)
{
    try
    {
        return fs.statSync(filePath).isFile();
    }
    catch (err)
    {
        return false;
    }
}

var build = new Build();

var command = build.getCommand();
assert(fileExists(command),"NAnt command not found: "+command);

build.run(["-help"]).then(function(){
	assert(true);
}).catch(function(code){
	assert(false,"Command failed");
});

