/* global __dirname */
var process = require("process"),
	path = require('path'),
	os = require('os'),
	childProcess = require('child_process'),
	colors = require('colors'),
	Promise = require('promise');

function Nant(){
	this.verbose = false;
	
	/**
	 * Gets path to the nant command.
	 * 
	 * */
	this.getCommand = function(){
		return path.join(__dirname,"tools\\bin\\NAnt.exe");
	}
	
	/**
	 * Gets default arguments taken from process
	 * 
	 * */
	this.getDefaultArguments = function(){
		var args = [];
		for(var arg in process.argv) { 
			args.push(process.argv[arg]); 
		}
		return args.splice(2);
	}
	
	this._spawnRun=function(command,args){
		var newArgs = ['/c',command].concat(args||[]);
		var self = this;
		return new Promise(function(resolve,reject){
			var proc = childProcess.spawn(process.env.comspec,newArgs,{
				cwd:process.cwd(),
				env: process.env,
				stdio:"inherit"
			});
			
			proc.on('close', function (code,sig) {
				
				if (code===0) {
					if (self.verbose) console.log(('Success!').white.greenBG);
					resolve();
				} else {
					if (self.verbose) console.log(('Failed with code ' + code).white.redBG);
					reject(code);
				} 
				
			});
			
		});
	}
	
	/**
	 * Executes nant with parameters
	 * 
	 * */
	this.run = function(args){
		if (typeof args === "undefined") {
			args = this.getDefaultArguments();
		}
		var command = this.getCommand();
		
		return this._spawnRun(command,args);
		
	}
}

module.exports = Nant;
